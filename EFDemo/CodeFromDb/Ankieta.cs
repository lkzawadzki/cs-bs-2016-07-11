namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Ankieta")]
    public partial class Ankieta
    {
        [Key]
        public int IDAnkieta { get; set; }

        public int IDKategoria { get; set; }

        public int IDSzkolenie { get; set; }

        [Required]
        [StringLength(255)]
        public string Opis { get; set; }

        public virtual KategoriaAnkiety KategoriaAnkiety { get; set; }

        public virtual Szkolenie Szkolenie { get; set; }
    }
}
