namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.TagiKategoria")]
    public partial class TagiKategoria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TagiKategoria()
        {
            KategoriaSzkolenia = new HashSet<KategoriaSzkolenia>();
        }

        [Key]
        [StringLength(15)]
        public string IDTag { get; set; }

        [Required]
        [StringLength(150)]
        public string Tekst { get; set; }

        public int KolejnoscWyswietlania { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KategoriaSzkolenia> KategoriaSzkolenia { get; set; }
    }
}
