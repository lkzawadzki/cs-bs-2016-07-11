namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Trener")]
    public partial class Trener
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Trener()
        {
            Szkolenie = new HashSet<Szkolenie>();
        }

        [Key]
        public int IDTrener { get; set; }

        [Required]
        [StringLength(75)]
        public string Nazwisko { get; set; }

        [Required]
        [StringLength(75)]
        public string Imie { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        public bool Widoczny { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Szkolenie> Szkolenie { get; set; }
    }
}
