namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Klient")]
    public partial class Klient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Klient()
        {
            Klient_OpisSzkolenia = new HashSet<Klient_OpisSzkolenia>();
            Uczestnik = new HashSet<Uczestnik>();
            Szkolenie = new HashSet<Szkolenie>();
        }

        [Key]
        [StringLength(25)]
        public string IDKlient { get; set; }

        [Required]
        [StringLength(100)]
        public string Nazwa { get; set; }

        [StringLength(150)]
        public string PelnaNazwa { get; set; }

        [StringLength(150)]
        public string Adres { get; set; }

        [StringLength(6)]
        public string KodPocztowy { get; set; }

        [StringLength(100)]
        public string Miasto { get; set; }

        [StringLength(50)]
        public string Kraj { get; set; }

        [StringLength(13)]
        public string NIP { get; set; }

        public bool WidocznyOtwarte { get; set; }

        public bool WidocznyZamkniete { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Klient_OpisSzkolenia> Klient_OpisSzkolenia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Uczestnik> Uczestnik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Szkolenie> Szkolenie { get; set; }
    }
}
