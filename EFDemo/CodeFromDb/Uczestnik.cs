namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Uczestnik")]
    public partial class Uczestnik
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Uczestnik()
        {
            Wynik = new HashSet<Wynik>();
        }

        [Key]
        public int IDUczestnik { get; set; }

        [StringLength(75)]
        public string Nazwisko { get; set; }

        [StringLength(75)]
        public string Imie { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(25)]
        public string Telefon { get; set; }

        public int IDSzkolenie { get; set; }

        [StringLength(25)]
        public string IDKlient { get; set; }

        public virtual Klient Klient { get; set; }

        public virtual Szkolenie Szkolenie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Wynik> Wynik { get; set; }
    }
}
