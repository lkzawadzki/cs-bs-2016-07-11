namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Promocje")]
    public partial class Promocje
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Promocje()
        {
            Szkolenie = new HashSet<Szkolenie>();
        }

        [Key]
        [StringLength(20)]
        public string IDPromocja { get; set; }

        [Required]
        [StringLength(150)]
        public string Nazwa { get; set; }

        [Required]
        public string Opis { get; set; }

        public int Rabat { get; set; }

        public DateTime? DataStart { get; set; }

        public DateTime? DataKoniec { get; set; }

        public bool Aktywna { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Szkolenie> Szkolenie { get; set; }
    }
}
