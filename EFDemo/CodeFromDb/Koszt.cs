namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Koszt")]
    public partial class Koszt
    {
        [Key]
        [StringLength(50)]
        public string Miasto { get; set; }

        [Column(TypeName = "money")]
        public decimal Podroz { get; set; }

        [Column(TypeName = "money")]
        public decimal Taxi { get; set; }

        [Column(TypeName = "money")]
        public decimal Hotel { get; set; }
    }
}
