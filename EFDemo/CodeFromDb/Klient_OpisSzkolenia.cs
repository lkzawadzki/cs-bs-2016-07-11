namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Klient_OpisSzkolenia")]
    public partial class Klient_OpisSzkolenia
    {
        [Key]
        public int IDOpis { get; set; }

        [Required]
        [StringLength(25)]
        public string Kategoria { get; set; }

        [Required]
        [StringLength(25)]
        public string Url { get; set; }

        [Required]
        [StringLength(200)]
        public string OpisSzkolenia { get; set; }

        [Required]
        [StringLength(25)]
        public string IDKlient { get; set; }

        public virtual Klient Klient { get; set; }
    }
}
