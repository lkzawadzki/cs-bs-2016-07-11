namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.KategoriaSzkolenia")]
    public partial class KategoriaSzkolenia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public KategoriaSzkolenia()
        {
            TagiKategoria = new HashSet<TagiKategoria>();
            TematSzkolenia = new HashSet<TematSzkolenia>();
        }

        [Key]
        [StringLength(15)]
        public string IDKategoria { get; set; }

        [Required]
        [StringLength(255)]
        public string Nazwa { get; set; }

        [Required]
        [StringLength(100)]
        public string TytulStrony { get; set; }

        [Required]
        [StringLength(2000)]
        public string Opis { get; set; }

        [StringLength(255)]
        public string Url { get; set; }

        public bool Widoczne { get; set; }

        public int? KolejnoscWyswietlania { get; set; }

        [StringLength(1000)]
        public string MetaOpis { get; set; }

        [StringLength(2000)]
        public string MetaSlowaKlucz { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TagiKategoria> TagiKategoria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TematSzkolenia> TematSzkolenia { get; set; }
    }
}
