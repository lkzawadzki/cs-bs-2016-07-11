namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Odpowiedz")]
    public partial class Odpowiedz
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Odpowiedz()
        {
            Odpowiedz1 = new HashSet<Odpowiedz>();
            Pytanie = new HashSet<Pytanie>();
        }

        [Key]
        public int IDOdpowiedz { get; set; }

        public int? Wartosc { get; set; }

        [Required]
        [StringLength(255)]
        public string Tekst { get; set; }

        public int? GrupaOdpowiedzi { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Odpowiedz> Odpowiedz1 { get; set; }

        public virtual Odpowiedz Odpowiedz2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Pytanie> Pytanie { get; set; }
    }
}
