namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Pytanie")]
    public partial class Pytanie
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Pytanie()
        {
            Odpowiedz = new HashSet<Odpowiedz>();
        }

        [Key]
        public int IDPytanie { get; set; }

        [Required]
        [StringLength(1000)]
        public string Tresc { get; set; }

        public int IDRodzaj { get; set; }

        public bool? LiczSrednia { get; set; }

        public int? IDGrupa { get; set; }

        public virtual Grupa Grupa { get; set; }

        public virtual RodzajPytania RodzajPytania { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Odpowiedz> Odpowiedz { get; set; }
    }
}
