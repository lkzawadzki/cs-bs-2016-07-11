namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Szkolenie")]
    public partial class Szkolenie
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Szkolenie()
        {
            Ankieta = new HashSet<Ankieta>();
            Uczestnik = new HashSet<Uczestnik>();
            Klient = new HashSet<Klient>();
        }

        [Key]
        public int IDSzkolenie { get; set; }

        [StringLength(15)]
        public string IDTemat { get; set; }

        public DateTime DataStart { get; set; }

        public DateTime DataKoniec { get; set; }

        [StringLength(150)]
        public string Adres { get; set; }

        [StringLength(6)]
        public string KodPocztowy { get; set; }

        [StringLength(100)]
        public string Miasto { get; set; }

        public bool Widoczne { get; set; }

        public bool Zatwierdzone { get; set; }

        [Column(TypeName = "money")]
        public decimal? Cena { get; set; }

        public int? IDTrener { get; set; }

        public bool CzyOtwarte { get; set; }

        [StringLength(20)]
        public string IDPromocja { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ankieta> Ankieta { get; set; }

        public virtual Promocje Promocje { get; set; }

        public virtual TematSzkolenia TematSzkolenia { get; set; }

        public virtual Trener Trener { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Uczestnik> Uczestnik { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Klient> Klient { get; set; }
    }
}
