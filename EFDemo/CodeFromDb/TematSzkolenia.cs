namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.TematSzkolenia")]
    public partial class TematSzkolenia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TematSzkolenia()
        {
            Szkolenie = new HashSet<Szkolenie>();
            Zapytanie = new HashSet<Zapytanie>();
            KategoriaSzkolenia = new HashSet<KategoriaSzkolenia>();
            TagiTematSzkolenia = new HashSet<TagiTematSzkolenia>();
        }

        [Key]
        [StringLength(15)]
        public string IDTemat { get; set; }

        [Required]
        [StringLength(255)]
        public string Nazwa { get; set; }

        [Required]
        [StringLength(100)]
        public string TytulStrony { get; set; }

        public int CzasTrwania { get; set; }

        [Required]
        [StringLength(2000)]
        public string Zakres { get; set; }

        [Required]
        public string PlanSzkolenia { get; set; }

        public bool Widoczne { get; set; }

        [StringLength(256)]
        public string Url { get; set; }

        [StringLength(1024)]
        public string MetaOpis { get; set; }

        [StringLength(2000)]
        public string MetaSlowaKlucz { get; set; }

        public int? KolejnoscWyswietlania { get; set; }

        [Column(TypeName = "money")]
        public decimal Cena { get; set; }

        [Column(TypeName = "money")]
        public decimal KosztMaterialow { get; set; }

        [Required]
        [StringLength(2000)]
        public string ZakresMD { get; set; }

        [Required]
        public string PlanSzkoleniaMD { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Szkolenie> Szkolenie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Zapytanie> Zapytanie { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KategoriaSzkolenia> KategoriaSzkolenia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TagiTematSzkolenia> TagiTematSzkolenia { get; set; }
    }
}
