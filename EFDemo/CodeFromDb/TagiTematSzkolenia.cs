namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.TagiTematSzkolenia")]
    public partial class TagiTematSzkolenia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TagiTematSzkolenia()
        {
            TematSzkolenia = new HashSet<TematSzkolenia>();
        }

        [Key]
        [StringLength(15)]
        public string IDTag { get; set; }

        [Required]
        [StringLength(150)]
        public string Tekst { get; set; }

        public int KolejnoscWyswietlania { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TematSzkolenia> TematSzkolenia { get; set; }
    }
}
