namespace CodeFromDb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Szkolenia.Zapytanie")]
    public partial class Zapytanie
    {
        [Key]
        public int IDZapytanie { get; set; }

        [StringLength(15)]
        public string IDTemat { get; set; }

        public DateTime DataWpisu { get; set; }

        public bool CzyOtwarte { get; set; }

        [Required]
        [StringLength(75)]
        public string Imie { get; set; }

        [Required]
        [StringLength(75)]
        public string Nazwisko { get; set; }

        [Required]
        [StringLength(25)]
        public string Telefon { get; set; }

        [Required]
        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(255)]
        public string Firma { get; set; }

        [StringLength(100)]
        public string FirmaMiasto { get; set; }

        [Required]
        [StringLength(255)]
        public string ProponowaneMiejsce { get; set; }

        public DateTime? ProponowanaDataStart { get; set; }

        public DateTime? ProponowanaDataKoniec { get; set; }

        public int LiczbaUczestnikow { get; set; }

        public string DoswiadczenieUczestnikow { get; set; }

        public string CeleSzkolenia { get; set; }

        public string SugerowaneZmiany { get; set; }

        public string Uwagi { get; set; }

        public bool Widoczne { get; set; }

        public bool Przetworzone { get; set; }

        public virtual TematSzkolenia TematSzkolenia { get; set; }
    }
}
