namespace CodeFromDb
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=ModelTest")
        {
        }

        public virtual DbSet<Ankieta> Ankieta { get; set; }
        public virtual DbSet<Grupa> Grupa { get; set; }
        public virtual DbSet<KategoriaAnkiety> KategoriaAnkiety { get; set; }
        public virtual DbSet<KategoriaSzkolenia> KategoriaSzkolenia { get; set; }
        public virtual DbSet<Klient> Klient { get; set; }
        public virtual DbSet<Klient_OpisSzkolenia> Klient_OpisSzkolenia { get; set; }
        public virtual DbSet<Koszt> Koszt { get; set; }
        public virtual DbSet<Odpowiedz> Odpowiedz { get; set; }
        public virtual DbSet<Promocje> Promocje { get; set; }
        public virtual DbSet<Pytanie> Pytanie { get; set; }
        public virtual DbSet<RodzajPytania> RodzajPytania { get; set; }
        public virtual DbSet<Szkolenie> Szkolenie { get; set; }
        public virtual DbSet<TagiKategoria> TagiKategoria { get; set; }
        public virtual DbSet<TagiTematSzkolenia> TagiTematSzkolenia { get; set; }
        public virtual DbSet<TematSzkolenia> TematSzkolenia { get; set; }
        public virtual DbSet<Trener> Trener { get; set; }
        public virtual DbSet<Uczestnik> Uczestnik { get; set; }
        public virtual DbSet<Wynik> Wynik { get; set; }
        public virtual DbSet<Zapytanie> Zapytanie { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Grupa>()
                .HasMany(e => e.KategoriaAnkiety)
                .WithMany(e => e.Grupa)
                .Map(m => m.ToTable("Grupa_KategoriaAnkiety", "Szkolenia").MapLeftKey("IDGrupa").MapRightKey("IDKategoria"));

            modelBuilder.Entity<KategoriaAnkiety>()
                .HasMany(e => e.Ankieta)
                .WithRequired(e => e.KategoriaAnkiety)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<KategoriaSzkolenia>()
                .HasMany(e => e.TagiKategoria)
                .WithMany(e => e.KategoriaSzkolenia)
                .Map(m => m.ToTable("KategoriaSzkolenia_TagiKategoria", "Szkolenia").MapLeftKey("IDKategoria").MapRightKey("IDTag"));

            modelBuilder.Entity<KategoriaSzkolenia>()
                .HasMany(e => e.TematSzkolenia)
                .WithMany(e => e.KategoriaSzkolenia)
                .Map(m => m.ToTable("Kategoryzacja", "Szkolenia").MapLeftKey("IDKategoria").MapRightKey("IDTemat"));

            modelBuilder.Entity<Klient>()
                .HasMany(e => e.Klient_OpisSzkolenia)
                .WithRequired(e => e.Klient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Klient>()
                .HasMany(e => e.Szkolenie)
                .WithMany(e => e.Klient)
                .Map(m => m.ToTable("Klient_Szkolenie", "Szkolenia").MapLeftKey("IDKlient").MapRightKey("IDSzkolenie"));

            modelBuilder.Entity<Koszt>()
                .Property(e => e.Podroz)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Koszt>()
                .Property(e => e.Taxi)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Koszt>()
                .Property(e => e.Hotel)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Odpowiedz>()
                .HasMany(e => e.Odpowiedz1)
                .WithOptional(e => e.Odpowiedz2)
                .HasForeignKey(e => e.GrupaOdpowiedzi);

            modelBuilder.Entity<Odpowiedz>()
                .HasMany(e => e.Pytanie)
                .WithMany(e => e.Odpowiedz)
                .Map(m => m.ToTable("Odpowiedz_Pytanie", "Szkolenia").MapLeftKey("IDOdpowiedz").MapRightKey("IDPytanie"));

            modelBuilder.Entity<RodzajPytania>()
                .HasMany(e => e.Pytanie)
                .WithRequired(e => e.RodzajPytania)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Szkolenie>()
                .Property(e => e.Cena)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Szkolenie>()
                .HasMany(e => e.Ankieta)
                .WithRequired(e => e.Szkolenie)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Szkolenie>()
                .HasMany(e => e.Uczestnik)
                .WithRequired(e => e.Szkolenie)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TagiTematSzkolenia>()
                .HasMany(e => e.TematSzkolenia)
                .WithMany(e => e.TagiTematSzkolenia)
                .Map(m => m.ToTable("TematSzkolenia_TagiTematSzkolenia", "Szkolenia").MapLeftKey("IDTag").MapRightKey("IDTemat"));

            modelBuilder.Entity<TematSzkolenia>()
                .Property(e => e.Cena)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TematSzkolenia>()
                .Property(e => e.KosztMaterialow)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Uczestnik>()
                .HasMany(e => e.Wynik)
                .WithRequired(e => e.Uczestnik)
                .WillCascadeOnDelete(false);
        }
    }
}
