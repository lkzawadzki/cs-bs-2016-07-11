﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFDemo.Classes;
using EFDemo.DataModel;
using System.Data.Entity;

namespace StudentsApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new NullDatabaseInitializer<StudentsContext>());
            //InsertStudent();
            //StudentQuery();
            //StudentQueryAndUpdate();
            //RetrieveWithStoredProcedure();
            //DeleteStudent();
            //AddStudentWithGrade();
            StudentGraphQuery();
            Console.ReadLine();
        }

        static void StudentGraphQuery()
        {
            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                //Student student = context.Students.Find(9);

                //eager loading
                //Student student = context.Students.Include(s => s.Grades).FirstOrDefault(s => s.Id == 9);

                //explicit loading
                Student student = context.Students.FirstOrDefault(s => s.Id == 9);

                //context.Entry(student).Collection(s => s.Grades).Load();

                Console.WriteLine(student.Name + " ma " + student.Grades.Count() + " ocen");

            }
        }

        static void AddStudentWithGrade()
        {
            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                var student = new Student()
                {
                    Name = "Adam",
                    GroupId = 1,
                    Passed = false,
                    DateOfBirth = new DateTime(1986, 5, 15)
                };

                var gradeOne = new Grade()
                {
                    Name = "Trója",
                    Value = 3
                };
                var gradeTwo = new Grade()
                {
                    Name = "Pała",
                    Value = 1
                };

                context.Students.Add(student);

                student.Grades.Add(gradeOne);
                student.Grades.Add(gradeTwo);

                context.SaveChanges();
            }
        }

        static void DeleteStudent()
        {
            int key = 5;

            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                //context.Students.SqlQuery("exec DeleteStudent {0}", key);

                context.Database.ExecuteSqlCommand("exec DeleteStudent @Id = {0}", key);
            }
        }

        static void RetrieveWithStoredProcedure()
        {
            int key = 6;

            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                //var students = context.Students
                    //.SqlQuery("select * from students where DateOfBirth >= '1/1/1992'")
                    //.ToList();

                var students = context.Students
                    .SqlQuery("exec FindStudent")
                    .ToList();

                foreach (var s in students)
                    Console.WriteLine(s.Name);
            }
        }

        static void RetrieveWithFind()
        {
            int key = 6;

            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                Student student = context.Students.Find(key);

                Console.WriteLine(student.Name);
            }
        }

        static void StudentQueryAndUpdate()
        {
            Student student;

            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                string name = "Maciek";

                student = context.Students.First(s => s.Name == name);
            }

            student.Name = "Kuba";

            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                context.Students.Attach(student);

                context.Entry(student).State = EntityState.Modified;

                context.SaveChanges();                
            }
        }

        static void StudentQuery()
        {
            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                string name = "Maciek";

                var students = context.Students.First(s => s.Name == name);

                Console.WriteLine(students.Name);

                //foreach (var s in students)
                //    Console.WriteLine(s.Name);
            }
        }

        static void InsertStudent()
        {
            Student x = new Student()
            {
                Name = "Robert",
                GroupId = 1,
                Passed = true,
                DateOfBirth = new DateTime(1990, 12, 10)
            };

            Student student2 = new Student()
            {
                Name = "Grzesiek",
                GroupId = 1,
                Passed = true,
                DateOfBirth = new DateTime(1995, 12, 10)
            };

            using (StudentsContext context = new StudentsContext())
            {
                context.Database.Log = Console.WriteLine;

                //context.Students.Add(student);

                context.Students.AddRange(new List<Student> { x, student2 });

                context.SaveChanges();
            }
        }
    }
}
