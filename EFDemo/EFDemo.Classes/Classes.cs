﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFDemo.Classes
{
    public class Student
    {
        public Student()
        {
            Grades = new List<Grade>();
        }

        public int Id { get; set; }
        public string Name { get; set; } 
        public bool Passed { get; set; }
        public Group Group { get; set; }
        public int GroupId { get; set; }
        public DateTime DateOfBirth { get; set; }

        public virtual List<Grade> Grades { get; set; }
    }

    public class Grade
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
        public Subject Subject { get; set; }
        //[Required]
        public Student Student { get; set; }
    }

    public enum Subject
    {
        Mathematics,
        Programming,
        Swimming
    }

    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<Student> Students { get; set; }
    }
}
