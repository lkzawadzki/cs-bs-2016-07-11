﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            ParsowaniePlikuTekstowego();
            Console.ReadLine();
        }

        static void ParsowaniePlikuTekstowego()
        {
            //wyswietl informacje w formacie "Imię: {0} Nazwisko: {1} Data urodzenia: {2}"

            string[] lines = File.ReadAllLines("dane.txt");
        }

        static void FunkcjonalnoscTablic()
        {
            int[] tablica = { 2, 3, 4, 3, 1};

            foreach (int i in tablica)
                Console.WriteLine(i);

            Console.WriteLine();

            Array.Reverse(tablica);

            foreach (int i in tablica)
                Console.WriteLine(i);

            Console.WriteLine();

            Array.Clear(tablica, 3, 2);

            foreach (int i in tablica)
                Console.WriteLine(i);
        }

        static void TablicaProstokatna()
        {
            int[,] matrix = new int[6, 6];

            int[][] myMatrix = new int[6][];

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    matrix[i, j] = i * j;
                }
            }

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Console.Write(matrix[i,j] + "\t");
                }
                Console.WriteLine();
            }
        }

        static void TablicaObiektow()
        {
            object[] tablica = new object[3];

            tablica[0] = 2;
            tablica[1] = 3.4f;
            tablica[2] = DateTime.Now;

            foreach(object obj in tablica)
            {
                Console.WriteLine("typ: {0}, wartość: {1}", obj.GetType(), obj);
            }
        }

        static void ProstaTablica()
        {
            //int[] tablica = new int[3];

            //tablica[0] = 12;
            //tablica[2] = 6;

            //int[] tablica = { 2, 3, 4 };

            var tablica = new[] { "jablko", "pomarancza", "banan" };

            foreach (var i in tablica)
            {
                Console.WriteLine(i);
            }

        }
    }
}
