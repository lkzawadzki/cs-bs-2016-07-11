﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsBasics
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 6;
            int b = 3;

            string s1 = "Hello";
            string s2 = "Hi";

            Console.WriteLine("a: {0}, b: {1}", a, b);
            Console.WriteLine("a: {0}, b: {1}", s1, s2);

            Swap(ref a, ref b);
            Swap(ref s1, ref s2);

            Console.WriteLine("a: {0}, b: {1}", a, b);
            Console.WriteLine("a: {0}, b: {1}", s1, s2);

            Point<double> p = new Point<double>(10, 10);

            Console.WriteLine("x: {0}, y: {1}", p.X, p.Y);

            p.Reset();

            Console.WriteLine("x: {0}, y: {1}", p.X, p.Y);

            Point<string> p1 = new Point<string>("x1", "y1");

            Console.ReadKey();
        }

        static void Swap<T>(ref T a, ref T b)
        {
            Console.WriteLine("Typ metody to: {0}, {1}", typeof(T), typeof(T).BaseType);

            T temp = b;

            b = a;

            a = temp;
        }
    }
}
