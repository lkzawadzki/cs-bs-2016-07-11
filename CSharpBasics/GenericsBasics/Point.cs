﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericsBasics
{
    class SomeClass
    {

    }

    interface ISomeInterface
    {

    }

    class Point<T> where T : SomeClass, ISomeInterface
    {
        private T x;
        private T y;

        public Point(T xVal, T yVal)
        {
            x = xVal;
            y = yVal;
        }

        //public T Add()
        //{
        //    //you shall not pass!
        //    //return x + y;
        //}

        public T X
        {
            get
            {
                return x;
            }
        }

        public T Y
        {
            get
            {
                return y;
            }
        }

        public void Reset()
        {
            x = default(T);
            y = default(T);
        }
    }
}
