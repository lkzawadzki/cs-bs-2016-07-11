﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car(name:  "Volvo");

            car.SpeedUp();

            Console.WriteLine(car.name);

            Console.ReadLine();
        }
    }

    class Car
    {
        public Car(string name, int speed = 10, int gear = 1)
        {
            Console.WriteLine("In master constructor");

            if (speed < -30)
                this.speed = -30;

            if (speed > 250)
                this.speed = 250;

            this.name = name;
        }

        public int gear;
        public string name;
        public int speed;

        public void SpeedUp()
        {
            for (int i = 0; i < 5; i++)
            {
                speed += 5;

                this.PrintState();
            }
        }

        public void PrintState()
        {
            Console.WriteLine("{0}, speed: {1} km/h", name, speed);
        }
    }
}
