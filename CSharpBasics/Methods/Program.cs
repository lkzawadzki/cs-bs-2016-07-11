﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metody
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 6;
            int b = 10;

            int sum = a + b;

            //int answer;

            //int ans = Add(a, b, out answer);

            //Console.WriteLine(answer);

            //string c;

            //bool d;

            //ProcessValues(out answer, out c, out d);

            //Console.WriteLine("{0} {1} {2}", answer, c, d);

            //string firstCountry = "Polska";

            //string secondCountry = "Węgry";

            //Console.WriteLine("{0} {1}", firstCountry, secondCountry);

            //SwapStrings(ref firstCountry, ref secondCountry);

            //Console.WriteLine("{0} {1}", firstCountry, secondCountry);

            //PrintMessage("Cześć", "Tomek");
            //PrintMessage("Cześć", author: "Janek");

            //var t2 = WypiszTablicę("Jan", "Zenon", "Maciek");

            //WypiszTablicę(t2);

            //Point aPoint = new Point(12, 15);
            //Point bPoint = new Point(10, 20);

            //Console.WriteLine("Punkt 1: x - {0}, y - {1}", aPoint.X, aPoint.Y);
            //Console.WriteLine("Punkt 2: x - {0}, y - {1}", bPoint.X, bPoint.Y);

            //ChangePoints(aPoint, bPoint);

            //Console.WriteLine("Punkt 1: x - {0}, y - {1}", aPoint.X, aPoint.Y);
            //Console.WriteLine("Punkt 2: x - {0}, y - {1}", bPoint.X, bPoint.Y);

            //StructWithReferenceType();

            Point aP = new Point(5, 10);

            aP.Display();

            CompletelyChangeMyPoint(ref aP);

            aP.Display(); 

            Console.ReadLine();
        }

        static void CompletelyChangeMyPoint(ref Point a)
        {
            a.X = 10;
            a.Y = 20;

            a = new Point(20, 40);
        }

        static void StructWithReferenceType()
        {
            Triangle tr1 = new Triangle("Pierwszy", 3, 5, 2);

            Triangle tr2 = tr1;

            tr1.Display();
            tr2.Display();

            tr2.Info.info = "nowa informacja";
            tr2.a = 7;

            tr1.Display();
            tr2.Display();
        }

        static void ChangePoints(Point a, Point b)
        {
            a.X = 24;
            a.Y = 25;
            b.Y = 16;
        }

        static int Add(int x, int y)
        {
            return x + y;
        }

        static string[] WypiszTablicę(string text, params string[] tablica)
        {
            for (int i = 0; i < tablica.Length; i++)
            {
                Console.WriteLine(tablica[i]);
                tablica[i] = tablica[i] + " Pracownik";
            }

            return tablica;
        }

        static void PrintMessage(string message, string author = "Maciek")
        {
            Console.WriteLine("{0}, autor: {1}", message, author);
        }

        //static int Add(int x, int y)
        //{
        //    return x + y;
        //}

        static int Add(int x, int y, out int ans)
        {
            ans = 10;

            return x + y;
        }

        static int Add(double x, double y)
        {
            int c = Convert.ToInt32(x + y); 
            return c;
        }

        static void ProcessValues(out int a, out string message, out bool condition)
        {
            a = 6;

            message = "Udało się";

            condition = true;
        }

        static void SwapStrings(ref string s1, ref string s2)
        {
            string temp = s1;

            s1 = s2;

            s2 = temp;
        }
    }
}
