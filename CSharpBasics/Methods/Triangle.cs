﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Metody
{
    struct Triangle
    {
        public Info Info;

        public int a, b, c;

        public Triangle(string info, int a, int b, int c)
        {
            this.Info = new Info(info);
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public void Display()
        {
            Console.WriteLine(Info.info, a, b, c);
        }
    }

    class Info
    {
        public string info;

        public Info(string info)
        {
            this.info = info;
        }
    }
}
