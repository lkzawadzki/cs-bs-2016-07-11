﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionalProgramming
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 1, 2, 6, 9, 14 };

            int sum = Add(array);

            Console.WriteLine("Suma to: " + sum);

            int sum2 = Sum(array);

            Console.WriteLine("Suma funkcjonalna to: " + sum2);

            Console.ReadLine();
        }

        static int Sum(int[] values, int i = 0)
        {
            if (values[i] == values[values.Length - 1])
                return values[values.Length - 1];
            return values[i] + Sum(values, i + 1);
        }

        static int Add(int[] values)
        {
            int sum = 0;

            foreach (int i in values)
            {
                sum += i;
            }

            return sum;
        }
    }
}
