﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumsAdnStructures
{
    class Program
    {
        static void Main(string[] args)
        {

            Wojskowy janusz = Wojskowy.General;

            TranferPayroll(janusz);

            DayOfWeek dzien = DayOfWeek.Monday;

            EvaluateEnum(ConsoleColor.Blue);

            Console.ReadLine();

        }

        static int? GetValueFromDatabase()
        {
            return null;
        }

        static void TypyNullable()
        {
            bool? value = null;

            int? valueInt = null;

            int?[] tablica = null;

            Nullable<int> dek = null;

            dek = GetValueFromDatabase() ?? 0;

        }

        

        static void EvaluateEnum(System.Enum e)
        {
            Console.WriteLine("Informacje o enumie {0}", e.GetType().Name);

            Console.WriteLine("Typ danych: {0}", Enum.GetUnderlyingType(e.GetType()));

            Array tablica = Enum.GetValues(e.GetType());

            Console.WriteLine("Ilość składowych {0}", tablica.Length);

            for (int i = 0; i < tablica.Length; i++)
            {
                Console.WriteLine("Nazwa: {0}, Wartość: {0:D}", tablica.GetValue(i));
            }
        }

        static void TranferPayroll(Wojskowy w)
        {
            switch (w)
            {
                case Wojskowy.General:
                    Console.WriteLine("Wysłaliśmy dolę generałowi");
                    break;
                case Wojskowy.Admiral:
                    Console.WriteLine("Wysłaliśmy dolę admirałowi");
                    break;
                case Wojskowy.Colonol:
                    Console.WriteLine("Wysłaliśmy dolę pułkownikowi");
                    break;
                case Wojskowy.Lieutanat:
                    Console.WriteLine("Wysłaliśmy dolę poroucznikowi");
                    break;
            }
        }
    }

    enum Wojskowy : byte
    {
        General = 10,
        Admiral = 1,
        Colonol = 9,
        Lieutanat = 12
    }
}
