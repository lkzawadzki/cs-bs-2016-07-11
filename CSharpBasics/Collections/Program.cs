﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Person(string name, int id)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return string.Format("Id: {1}, Name: {0}", Name, Id);
        }
    }

    class SortByName : IComparer<Person>
    {
        public int Compare(Person x, Person y)
        {
            return String.Compare(x.Name, y.Name);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            GetDictionary();

            Console.ReadLine();
        }
        
        static void GetDictionary()
        {
            Dictionary<Guid, Person> dict = new Dictionary<Guid, Person>()
            {
                {Guid.NewGuid(), new Person("Marcin", 1) },
                {Guid.NewGuid(), new Person("Robert", 2) }
            };

            foreach(var item in dict)
            {
                Console.WriteLine("Key: {0}, Value: {1}", item.Key, item.Value.ToString());
            }
            
        }

        static void GetSortedSet()
        {
            SortedSet<Person> sortedSet = new SortedSet<Person>(new SortByName())
            {
                new Person("Szymon", 3),
                new Person("Monika", 2),
                new Person("Kuba", 1),
                new Person("Maciek", 2),
                new Person("Damian", 3)
            };

            foreach (var item in sortedSet)
            {
                Console.WriteLine("Id: {1}, Name: {0}", item.Name, item.Id);
            }

            Console.WriteLine();

            sortedSet.Add(new Person("Eryk", 4));

            foreach (var item in sortedSet)
            {
                Console.WriteLine("Id: {1}, Name: {0}", item.Name, item.Id);
            }
        }

        static void GetQueue()
        {
            Queue<Person> queue = new Queue<Person>();

            queue.Enqueue(new Person("Marian", 3));
            queue.Enqueue(new Person("Monika", 2));

            Console.WriteLine("Pierwszy/a w kolejce jest: {0}", queue.Peek().Name);

            ProcessPerson(queue.Dequeue());
            ProcessPerson(queue.Dequeue());

            try
            {
                ProcessPerson(queue.Dequeue());
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        static void ProcessPerson(Person p)
        {
            Console.WriteLine("Osoba {0} została obsłużona", p.Name);
        }

        static void GetStack()
        {
            Stack<Person> stack = new Stack<Person>();

            stack.Push(new Person("Marian", 3));
            stack.Push(new Person("Monika", 2));

            Console.WriteLine("Pierwszy na stosie jest: {0}", stack.Peek().Name);
            Console.WriteLine("Zrzucamy ze stosu: {0}", stack.Pop().Name);

            Console.WriteLine("Pierwszy na stosie jest: {0}", stack.Peek().Name);
            Console.WriteLine("Zrzucamy ze stosu: {0}", stack.Pop().Name);

            try
            {
                Console.WriteLine("Pierwszy na stosie jest: {0}", stack.Peek());
                Console.WriteLine("Zrzucamy ze stosu: {0}", stack.Pop());
            }
            catch (InvalidOperationException ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        //kolekcja generyczna
        static void GetList()
        {
            List<Person> list = new List<Person>();

            list.Add(new Person("Kuba", 1));

            list.AddRange(new List<Person> { new Person("Maciek", 2), new Person("Damian", 3) });

            Person p = new Person("Szymon", 6);

            list.Add(p);

            foreach (var item in list)
            {
                Console.WriteLine(item.Id + " " + item.Name);
            }

            Console.WriteLine(list.IndexOf(p));
        }

        //kolekcja niegeneryczna
        static void GetArrayList()
        {
            ArrayList arrayList = new ArrayList();

            arrayList.Add(2);
            arrayList.AddRange(new string[] { "France", "Germany", "Wales" });

            foreach (var s in arrayList)
            {
                Console.WriteLine("Array list item: {0}", s);
            }
        }

        static void GetArray()
        {
            string[] array = { "Portugal", "France", "Germany", "Wales" };
            
            foreach (string s in array)
            {
                Console.WriteLine("Array item: {0}", s);
            }
        }
    }
}
