﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MulticastDelegate
{
    class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Person(string name, int id)
        {
            Id = id;
            Name = name;
        }

        public override string ToString()
        {
            return string.Format("Id: {1}, Name: {0}", Name, Id);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //Przykład delegata multicast
            //Message del = Print;
            //del += Draw;
            //del += Show;
            //del -= Draw;
            //del("Wiadomość");

            ObservableCollection<Person> people = new ObservableCollection<Person>()
            {
                new Person("Zenon", 1),
                new Person("Magdalena", 2)
            };

            people.CollectionChanged += People_CollectionChanged;

            people.Add(new Person("Joanna", 5));

            people.RemoveAt(1);

            Console.ReadLine();
        }

        private static void People_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Console.WriteLine("Akcja eventu: {0}", e.Action);

            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                Console.WriteLine("Stara lista osób:");
                foreach (Person p in e.OldItems)
                    Console.WriteLine(p.ToString());
                Console.WriteLine();
            }

            if(e.Action == NotifyCollectionChangedAction.Add)
            {
                Console.WriteLine("Nowa lista osób:");
                foreach (Person p in e.NewItems)
                    Console.WriteLine(p.ToString());
                Console.WriteLine();
            }
        }

        static void Print(string message)
        {
            Console.WriteLine("Printing: {0}", message);
        }

        static void Draw(string message)
        {
            Console.WriteLine("Drawing: {0}", message);
        }

        static void Show(string message)
        {
            Console.WriteLine("Showing: {0}", message);
        }
    }

    delegate void Message(string message);
}
