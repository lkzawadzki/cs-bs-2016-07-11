﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorsAndExceptions
{
    class CarOverheatedException : ApplicationException
    {
        private string message = "";
        public DateTime DateStamp { get; set; }
        public string Cause { get; set; }

        public CarOverheatedException()
        {

        }

        public CarOverheatedException(string message, DateTime date, string cause)
        {
            this.message = message;
            this.DateStamp = date;
            this.Cause = cause;
        }

        public override string Message
        {
            get
            {
                return string.Format("Problem z samochodem", message);
            }
        }
    }
}
