﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorsAndExceptions
{
    class Radio
    {
        public void Power(bool on)
        {
            if (on)
                Console.WriteLine("Radio is on");
            else
                Console.WriteLine("Radio is off");

        }
    }
}
