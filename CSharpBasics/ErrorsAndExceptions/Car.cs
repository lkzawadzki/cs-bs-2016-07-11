﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorsAndExceptions
{
    class Car
    {
        public const int MaxSpeed = 100;

        public int Speed { get; set; }
        public string Name { get; set; }

        private Radio radio = new Radio();

        private bool IsDead;

        public Car()
        {

        }

        public Car(string name, int speed)
        {
            Speed = speed;
            Name = name;
        }

        public void TurnOnRadio(bool state)
        {
            radio.Power(state);
        }

        public void Accelerate(int delta)
        {
            if (delta < 0)
            {
                throw new ArgumentOutOfRangeException("delta", "musi być większa od 0");
            }

            if (IsDead)
            {
                Console.WriteLine("Samochód {0} nie działa", Name);
            }
            else
            {
                Speed += delta;

                if (Speed > MaxSpeed)
                {
                    Console.WriteLine("Samochód {0} przekroczył prędkość", Name);
                    Speed = 0;
                    IsDead = true;
                    CarOverheatedException ex = 
                        new CarOverheatedException(
                            string.Format("Auto {0} się przegrzało", Name), 
                            DateTime.Now, 
                            "za szybko chciałeś jechać");

                    throw ex;
                }
                else
                {
                    Console.WriteLine("Samochód {0} jedzie z prędkością", Speed);
                }
            }
        }
    }
}
