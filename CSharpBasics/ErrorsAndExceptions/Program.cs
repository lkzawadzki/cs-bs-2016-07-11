﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ErrorsAndExceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("Mazda", 60);

            car.TurnOnRadio(true);

            try
            {
                for (int i = 0; i <= 10; i++)
                {
                    car.Accelerate(10);
                }
            }
            catch (CarOverheatedException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.DateStamp);
                Console.WriteLine(ex.Cause);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                car.TurnOnRadio(false);
            }

            Console.ReadLine();
        }
    }
}
