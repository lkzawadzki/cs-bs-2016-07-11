﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesAndRelatedStuff
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>
            {
                new Employee(1, "Adam", 5000, 3),
                new Employee(2, "Bartosz", 3000, 1),
                new Employee(3, "Katarzyna", 8000, 4),
                new Employee(4, "Marzena", 4500, 7)
            };

            //Employee.ToFire del = new Employee.ToFire(FireBySalary);

            Employee.FireEmployee(employees, (Employee x) => x.Salary > 4999);

            //Predicate<Employee> predicate = new Predicate<Employee>(FindEmployee);

            //Employee emp = employees.Find(delegate (Employee employee) { return employee.Id == 1; } );
            Employee emp = employees.Find(employee => employee.Id == 1);

            Console.WriteLine(emp.Name);

            IEnumerable<string> stringTable = employees.Select(employee => "Name: " + employee.Name);

            foreach (var item in stringTable)
            {
                Console.WriteLine(item);
            }

            Func<int, int, string> selector = (x1, x2) => "Suma to: " + (x1 + x2).ToString();

            string result = selector(15, 20);

            Console.WriteLine(result);

            Console.ReadLine();
        }

        //public static bool FindEmployee(Employee employee)
        //{
        //    return employee.Id == 1;
        //}

        //public static bool FireBySalary(Employee employee)
        //{
        //    return employee.Salary > 4999;
        //}
    }

    class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Salary { get; set; }
        public int Experience { get; set; }

        public Employee(int id, string name, int salary, int experience)
        {
            Id = id;
            Name = name;
            Salary = salary;
            Experience = experience;
        }

        public static void FireEmployee(List<Employee> employees, ToFire isFireable)
        {
            foreach (Employee employee in employees)
            {
                if (isFireable(employee))
                {
                    Console.WriteLine("{0} został zwolniony!", employee.Name);
                }
            }
        }

        public delegate bool ToFire(Employee employee);

        //stare i niedobre
        //public static void FireEmployee(List<Employee> employees)
        //{
        //    foreach (Employee employee in employees)
        //    {
        //        if (employee.Experience < 2)
        //        {
        //            Console.WriteLine("{0} został zwolniony!", employee.Name);
        //        }
        //    }
        //}
    }
}
