﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractInterface
{
    class Circle : Shape
    {
        public Circle() { }

        public Circle(string label) : base(label) { }

        public Circle(string label, int radius) : base(label)
        {
            Radius = radius;
        }

        public int Radius { get; set; }

        public override void Draw()
        {
            Console.WriteLine("Rysowanie okręgu: {0}", Label);
        }

        public void GetRadius()
        {
            Console.WriteLine("Promien okręgu to {0}", Radius);
        }
    }
}
