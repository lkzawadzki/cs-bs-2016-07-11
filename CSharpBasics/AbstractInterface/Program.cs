﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            //Rectangle rec = new Rectangle("prostokąt");

            //rec.Draw();

            //Circle cir = new Circle("nasz okrąg");

            //cir.Draw();

            //object[] shapes = { new Rectangle("lab1"), new Circle("circle number one"), new Rectangle("czerwony"), "string"};

            Shape[] shapes = { new Rectangle("lab1"), new Circle("circle number one", 25), new Rectangle("czerwony")};

            foreach (object s in shapes)
            {
                //Shape temp = s as Shape;

                //if (temp != null)
                //    temp.Draw();

                //if (s is Shape)
                //    ((Shape)s).Draw();

                if (s is Circle)
                    ((Circle)s).GetRadius();
            }

            Console.ReadLine();
        }
    }
}
