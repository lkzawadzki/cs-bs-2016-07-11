﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encapsualation
{
    class Employee
    {
        private float payroll;
        private string name;
        private int id;
        private static string securityCode;

        public Employee(int id, string name, float payroll, string securityNumber, int age)
        {
            Id = id;
            Name = name;
            Payroll = payroll;
            SecurityCode = securityNumber;
            Age = age;
        }

        public void GiveRaise(float amount)
        {
            Payroll += amount;
        }

        public int Age { get; set; }

        public static string SecurityCode
        {
            get { return securityCode;  }
            set { securityCode = value; }
        }

        public int Id
        {
            get { return id; }
            set { id = value;  }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public float Payroll
        {
            get
            {
                return payroll;
            }
            set
            {
                if (value < 1500)
                {
                    payroll = 1500;
                }
                else
                {
                    payroll = value;
                }
            }
        }
    }
}
