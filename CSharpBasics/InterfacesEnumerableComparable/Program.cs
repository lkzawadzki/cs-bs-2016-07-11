﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesEnumerableComparable
{
    class Program
    {
        static void Main(string[] args)
        {
            Garage garage = new Garage();

            //foreach (var item in garage.GetCars())
            //{
            //    Console.WriteLine(item.Name + " " + item.Speed);
            //}

            //Car car;

            //IEnumerator i = garage.GetEnumerator();

            //i.MoveNext();
            //car = (Car)i.Current;
            //Console.WriteLine(car.Name + " " + car.Speed);

            //i.MoveNext();
            //car = (Car)i.Current;
            //Console.WriteLine(car.Name + " " + car.Speed);

            //i.Reset();
            //i.MoveNext();
            //car = (Car)i.Current;
            //Console.WriteLine(car.Name + " " + car.Speed);

            //foreach (Car item in garage)
            //{
            //    Console.WriteLine(item.Name + " " + item.Speed);
            //}

            //Console.WriteLine();

            //foreach (Car item in garage.GetCarsInOrder(true))
            //{
            //    Console.WriteLine(item.Name + " " + item.Speed);
            //}

            Car[] cars = { new Car(3, "Volvo", 100), new Car(1, "Mazda", 100), new Car(2, "Mazda", 150) };

            Array.Sort(cars, Car.SortBySpeed);

            foreach (Car item in cars)
            {
                Console.WriteLine("{0}: {1} - {2}", item.Id, item.Name, item.Speed);
            }

            Console.ReadKey();
        }
    }
}
