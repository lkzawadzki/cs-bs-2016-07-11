﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesEnumerableComparable
{
    class Garage : IEnumerable
    {
        private Car[] cars = new Car[4];

        public Garage()
        {
            cars[0] = new Car("Mazda", 180);
            cars[1] = new Car("Volvo", 210);
            cars[2] = new Car("Fiat Multipla", 100);
            cars[3] = new Car("Ford Mondeo", 150);
        }

        public Car[] GetCars()
        {
            return cars;
        }

        public IEnumerator GetEnumerator()
        {
            foreach(Car c in cars)
            {
                yield return c;
            }
        }

        public IEnumerable GetCarsInOrder(bool order)
        {
            if (order)
            {
                for (int i = cars.Length; i != 0; i--)
                {
                    yield return cars[i - 1];
                }
            }
            else
            {
                foreach(Car c in cars)
                {
                    yield return c;
                }
            }
        }

        //public IEnumerator GetEnumerator()
        //{
        //    return cars.GetEnumerator();
        //}
    }
}
