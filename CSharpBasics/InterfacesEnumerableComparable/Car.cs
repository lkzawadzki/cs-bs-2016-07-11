﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesEnumerableComparable
{
    class Car : IComparable
    {
        public int Id { get; set; }
        public int Speed { get; set; }
        public string Name { get; set; }

        public Car(string name, int speeed)
        {
            Speed = speeed;
            Name = name;
        }

        public Car(int id, string name, int speed)
        {
            Id = id;
            Name = name;
            Speed = speed;
        }

        public int CompareTo(object obj)
        {
            Car temp = obj as Car;

            if (temp != null)
                return this.Id.CompareTo(temp.Id);
            else
                throw new ArgumentException("Parametr obj is not a car");
        }

        public static IComparer SortBySpeed
        {
            get
            {
                return new SpeedComparer();
            }
        }
    }
}
