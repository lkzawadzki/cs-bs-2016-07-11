﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfacesEnumerableComparable
{
    class SpeedComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            Car c1 = x as Car;
            Car c2 = y as Car;

            if (c1 != null && c2 != null)
            {
                if (c1.Speed > c2.Speed)
                    return 1;
                else if (c1.Speed == c2.Speed)
                    return 0;
                else
                    return -1;
            }
            else
            {
                throw new ArgumentException("Któryś z obiektów nie był samochodem");
            }
        }
    }
}
