﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlStatements
{
    class Motorcycle
    {
        public int driverIntensity;
        // New members to represent the name of the driver.
        public string name;
        public void SetDriverName(string name)
        {
            this.name = name;
        }
    }

    class Program
    {
        
        static void Main(string[] args)
        {
            //IfExample();

            //IfElseElseIfExample();

            //TernaryOperator();

            //BiggestOfThree(1, 4, 3);

            //SwitchExample();

            SwitchCharExample();

            Console.ReadLine();
        }

        static void SwitchCharExample()
        {
            Console.WriteLine("Jak poprawnie zadeklarowac integer w c#");

            Console.WriteLine("a. int 1x = 10");
            Console.WriteLine("b. int c = 25");
            Console.WriteLine("c. int float = 10.0f");
            Console.WriteLine("d. var int = 1");

            char ans = (char)Console.Read();

            switch (ans)
            {
                case 'a': Console.WriteLine("Wrong"); break;
                case 'b': Console.WriteLine("Right answer"); break;
                case 'c': Console.WriteLine("Wrong"); break;
                case 'd': Console.WriteLine("Wrong"); break;
                default: Console.WriteLine("Invalid letter"); break;
            }
        }

        static void SwitchExample()
        {
            //int dzien = (int)DateTime.Now.DayOfWeek;

            int dzien = 6;

            switch (dzien)
            {
                case 1:
                    Console.WriteLine("Poniedziałek");
                    break;
                case 2:
                    Console.WriteLine("Wtorek");
                    break;
                case 3:
                    Console.WriteLine("Środa");
                    break;
                case 4:
                    Console.WriteLine("Czwartek");
                    break;
                case 5:
                    Console.WriteLine("Piątek");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Weekend");
                    break;
                default:
                    Console.WriteLine("Wartość była nieprawidłowa");
                    break;
            }

        }

        static void BiggestOfThree(int x, int y, int z)
        {
            Console.WriteLine("{0} {1} {2}", x, y, z);

            int[] tablica = { x, y, z };

            int max = tablica.Max();

            if (x > y)
            {
                if (x > z)
                {
                    Console.WriteLine(x + " jest największe");
                }
                else
                {
                    Console.WriteLine(z + " jest największe");
                }
            }
            else if (y > z)
            {
                Console.WriteLine(y + " jest największe");
            }
            else
            {
                Console.WriteLine(z + " jest największe");
            }

        }

        static void TernaryOperator()
        {
            int value = 100;

            if (value.ToString() == "100")
            {
                value = 1;
            }
            else
            {
                value = -1;
            }

            value = value.ToString() == "100" ? 1 : -1;

            Console.WriteLine(value);
        }

        static void IfElseElseIfExample()
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
            {
                Console.WriteLine("Dopiero poniedziałek :(");
            }
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                Console.WriteLine("Jeszcze dwa dni do weekendu");
            }
            else if (DateTime.Now.DayOfWeek >= DayOfWeek.Friday)
            {
                Console.WriteLine("Już jest weekend");
            }
            else
            {
                Console.WriteLine("Wtorek lub czwartek");
            }
        }

        static void IfExample()
        {
            string myString = "Politechnika";

            if (!string.IsNullOrEmpty(myString) || myString.StartsWith("P"))
            {
                Console.WriteLine(myString + " has length " + myString.Length);
            }
        }
    }
}
