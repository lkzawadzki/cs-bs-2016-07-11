﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager manager = new Manager("Maciek", 10, 20000, 45, 15);

            Console.WriteLine(manager.ID);

            Console.WriteLine(manager.CarColor());

            Console.WriteLine("Obecna płaca managera {0}", manager.Pay);
            Console.WriteLine("Obecne udziały managera {0}", manager.OwningPercentage);

            manager.GiveBonus(5000);

            Console.WriteLine("Nowa płaca managera {0}", manager.Pay);
            Console.WriteLine("Nowe udziały managera {0}", manager.OwningPercentage);
            
            Console.ReadLine();
        }
    }
}
