﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    abstract class Employee
    {
        protected string empName;
        private int empID;
        private float currPay;
        private int empAge;

        public Employee()
        {
            Console.WriteLine("Employee default constructor");
        }

        public Employee(string name, int id, float pay) : this(name, 0, id, pay)
        { }

        public Employee(string name, int age, int id, float pay)
        {
            Console.WriteLine("Employee constructor");
            Name = name;
            Age = age;
            ID = id;
            Pay = pay;
        }

        public virtual void GiveBonus(float amount)
        {
            currPay += amount;
        }

        public void DisplayStats()
        {
            Console.WriteLine("Name: {0}", Name);
            Console.WriteLine("ID: {0}", ID);
            Console.WriteLine("Age: {0}", Age);
            Console.WriteLine("Pay: {0}", Pay);
        }

        public string GetName()
        {
            return empName;
        }

        public void SetName(string name)
        {
            if (name.Length > 15)
                Console.WriteLine("Musi byc dluzsze od 15 znakow");
            else
                empName = name;
        }

        public string Name
        {
            get { return empName; }
            set
            {
                if (value.Length > 15)
                    Console.WriteLine("Musi miec co najmniej 16 znakow");
                else
                    empName = value;
            }
        }

        public int ID
        {
            get { return empID; }
            set { empID = value; }
        }
        public float Pay
        {
            get { return currPay; }
            set { currPay = value; }
        }

        public int Age
        {
            get { return empAge; }
            set { empAge = value; }
        }
    }
}
