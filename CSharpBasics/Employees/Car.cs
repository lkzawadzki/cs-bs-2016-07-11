﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class Car
    {
        private string Color;

        public Car(string color)
        {
            Color = color;
        }

        public string GetColor()
        {
            return Color;
        }
    }
}
