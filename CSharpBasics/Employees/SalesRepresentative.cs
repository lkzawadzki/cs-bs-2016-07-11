﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class SalesRepresentative : Employee
    {
        public int NumberOfSales { get; set; }

        public SalesRepresentative(string name, int id, float pay, int age, int sales) : base(name, age, id, pay)
        {
            Console.WriteLine("Manager constructor");
            this.NumberOfSales = sales;
        }

        public override sealed void GiveBonus(float amount)
        {
            int bonus = 0;

            if (NumberOfSales > 10 && NumberOfSales < 25)
                bonus = 5;
            else if (NumberOfSales > 25 && NumberOfSales < 50)
                bonus = 10;
            else
                bonus = 20;

            base.GiveBonus(bonus * amount);
        }
    }
}
