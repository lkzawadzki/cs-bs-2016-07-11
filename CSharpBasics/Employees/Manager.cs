﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Employees
{
    class Manager : Employee
    {
        public int OwningPercentage { get; set; }

        public Manager(string name, int id, float pay, int age, int percentage) : base(name, age, id, pay)
        {
            Console.WriteLine("Manager constructor");
            this.OwningPercentage = percentage;

            empName = "Robert";
        }

        private Car car = new Car("złoty");

        public string CarColor()
        {
            return car.GetColor();
        }

        public override void GiveBonus(float amount)
        {
            base.GiveBonus(amount);

            //OwningPercentage = Convert.ToInt32(OwningPercentage * 1.1);

            OwningPercentage = Convert.ToInt32(Math.Ceiling(OwningPercentage * 1.1));
        }
    }
}
