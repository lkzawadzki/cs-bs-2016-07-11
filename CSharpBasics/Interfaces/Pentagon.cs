﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Pentagon : IPrintToJpg, IPrintToPdf, IPrintToPrinter, IGetAreaInSquareFoots
    {
        public int GetArea()
        {
            return 6;
        }

        public int GetAreaInSquareFoots()
        {
            return 54;
        }

        void IPrintToPdf.Print()
        {
            Console.WriteLine("Printing pentagon to pdf...");
        }

        void IPrintToPrinter.Print()
        {
            Console.WriteLine("Printing pentagon to printer...");
        }

        void IPrintToJpg.Print()
        {
            Console.WriteLine("Printing pentagon to jpg...");
        }
    }
}
