﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    abstract class Shape
    {
        private string _label;

        public string Label
        {
            get
            {
                return _label;
            }
        }

        public Shape(string label = "nolabel")
        {
            _label = label;
        }

        public abstract void Draw();
    }
}
