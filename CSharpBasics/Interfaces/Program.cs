﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{

    class Program
    {
        static void Main(string[] args)
        {
            //Rectangle rec = new Rectangle("nasz prostokąt");

            //Console.WriteLine("Liczba punktów: {0}", rec.GetNumberOfPoints());

            //Shape[] shapes = { new Rectangle("rec1"), new Circle("okrąg"), new Rectangle("rec2") };

            //foreach (Shape s in shapes)
            //{
            //    //IPoints point = s as IPoints;

            //    if (s is IPoints)
            //        Console.WriteLine("Liczba punktów: {0}", ((IPoints)s).GetNumberOfPoints());
            //}

            //IPoints point = GetFirstShapeWithPoints(shapes);

            //Console.WriteLine(point.GetNumberOfPoints());

            //Circle cir1 = new Circle("do drukowania");

            //PrintToPrinter(cir1);

            Pentagon pen = new Pentagon();

            //pen.Print();

            ((IPrintToPrinter)pen).Print();

            ((IPrintToPdf)pen).Print();

            ((IPrintToJpg)pen).Print();

            Console.ReadLine();
        }

        static void PrintToPrinter(IPrintToPrinter ptp)
        {
            ptp.Print();
        }

        static IPoints GetFirstShapeWithPoints(Shape[] shapes)
        {
            foreach (Shape s in shapes)
            {
                if (s is IPoints)
                    return s as IPoints;
            }
            return null;
        }
    }
}
