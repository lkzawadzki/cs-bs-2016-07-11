﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    interface IPoints
    {
        byte GetNumberOfPoints();
    }

    interface IPrintToPrinter
    {
        void Print();
    }

    interface IPrintToPdf
    {
        void Print();
    }

    interface IPrintToJpg
    {
        void Print();
    }
}
