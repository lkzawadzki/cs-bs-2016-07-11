﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Rectangle : Shape, IPoints
    {
        public Rectangle()
        {

        }

        public Rectangle(string label) : base(label) { }

        public override void Draw()
        {
            Console.WriteLine("Drawing Rectangle: {0}", Label);
        }

        public byte GetNumberOfPoints()
        {
            return 4;
        }
    }
}
