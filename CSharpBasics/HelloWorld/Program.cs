﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.Numerics;

namespace HelloWorld
{
    class Program
    {
        
        static int Main()
        {
            //Console.WriteLine("Hello World!");

            //string[] args = Environment.GetCommandLineArgs();

            //foreach (string s in args)
            //{
            //    Console.WriteLine(s);
            //}

            //FormatowanieLiczb();

            //bool b = new bool();

            //int i = new int();

            //double d = new double();

            //DateTime dt = new DateTime();

            //Console.WriteLine("int: {0}, bool: {1}, double: {2}, datetime: {3}", i, b, d, dt = DateTime.Now);

            //metody System.Object
            //Console.WriteLine("GetHashCode() dla liczby 12: {0}", 12.GetHashCode());

            //Console.WriteLine("Equals(23) dla liczby 12: {0}", 12.Equals(23));

            //Console.WriteLine("ToString() dla liczby 12: {0}", 12.ToString());

            //Console.WriteLine("GetType() dla liczby 12: {0}", 12.GetType());

            ////maks i min dla wartości
            //Console.WriteLine("Max dla inta: {0}", int.MaxValue);
            //Console.WriteLine("Min dla inta: {0}", int.MinValue);

            //Console.WriteLine("Max dla double: {0}", double.MaxValue);
            //Console.WriteLine("Min dla double: {0}", double.MinValue);

            //BasicCharCapability();

            //ParseFromStrings();

            //DateTypes();

            //DuzeIntegery();

            //ProstaManipulacjaStringow();

            //string s1 = "Hello";
            //string s2 = "World";

            //string s3 = string.Concat(s1, s2);

            //Console.WriteLine(s3);

            //string s4 = s1 + s2;

            //Console.WriteLine(s4);

            //ZnakiSpecjalne();

            //PorownanieStringow();

            //StringImmutability2();

            //StringBuilderDemo();

            NiejawnaDeklaracjaZmiennych();

            Console.ReadLine();

            return 0;
        }



        static void NiejawnaDeklaracjaZmiennych()
        {
            //var name = "Maciek";           

            //Console.WriteLine(name);

            int[] tablicaIntow = { 10, 30, 21, 3, 4, 5, 7 };

            var subset = from i in tablicaIntow
                         where i > 8
                         select i;
            
            foreach(var i in subset)
            {
                Console.WriteLine(i);
            }

            var subset2 = tablicaIntow
                            .Where(n => n > 8)
                            .OrderBy(n => n);

            foreach(int i in subset2)
            {
                Console.WriteLine(i);
            }

        }

        static void OverflowCheck()
        {
            Console.WriteLine("Max short: " + short.MaxValue);
            Console.WriteLine("Max int: " + int.MaxValue);

            //Rozszerzanie konwersji
            short s1 = 20000;
            short s2 = 20000;

            Console.WriteLine((short)Add(s1, s2));

            try
            {
                checked
                {
                    short sum = (short)Add(s1, s2);
                    Console.WriteLine(sum);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            //long l1 = 200L;
            //long l2 = 1500L;

            //Console.WriteLine(Add(l1, l2));
        }

        static int Add(short x, short y)
        {
            return x + y;
        }

        static int Add(int x, int y)
        {
            return x + y;
        }

        static void StringBuilderDemo()
        {
            StringBuilder builder = new StringBuilder("Kraje Europy:\n");

            builder.Append("Portugalia\n");
            builder.Append("Francja\n");
            builder.Append("Walia\n");
            builder.Append("Niemcy\n");

            Console.WriteLine(builder.ToString());

            builder.Replace("Portugalia", "Polska");

            Console.WriteLine(builder.ToString());

        }

        static void StringImmutability()
        {
            string name = "Kuba";

            Console.WriteLine(name);

            string upperName = name.ToUpper();

            Console.WriteLine(upperName);

            Console.WriteLine(name);
        }

        static void StringImmutability2()
        {
            string s = "Kuba";

            s = "Jacek";

            Console.WriteLine(s);
        }

        static void PorownanieStringow()
        {
            string s1 = "Hello";

            string s2 = "Hi";

            Console.WriteLine(s1 == s2);
            Console.WriteLine(s1 == "Hello");
            Console.WriteLine(s1 == "HELLO");
            Console.WriteLine(s1.Equals("Hi"));
            Console.WriteLine(s1.Equals("Hello"));
        }

        static void ZnakiSpecjalne()
        {
            string zTabulacjami = "Łukasz\tZawadzki\tTrener\tInfotraining\a";

            string zCudzyslowiem = "Wiadomość to \"Witaj świecie\"";

            string zNowaLinijka = "Wiadomość\n";

            string sciezka = "c:\\Users\\Piotr";

            Console.WriteLine(sciezka);

            Console.WriteLine(zNowaLinijka);

            Console.WriteLine(zCudzyslowiem);

            Console.WriteLine(zTabulacjami);
        }


        static void ProstaManipulacjaStringow()
        {
            string name = "Kuba";

            Console.WriteLine(name);
            Console.WriteLine(name.Length);
            Console.WriteLine(name.ToUpper());
            Console.WriteLine(name.ToLower());
            Console.WriteLine(name.Contains("ba"));
            Console.WriteLine(name.StartsWith("Ku"));
            Console.WriteLine(name.Substring(2).StartsWith("ba"));
        }

        static void DuzeIntegery()
        {
            BigInteger big = BigInteger.Parse("999999999999999999999999999999999999");

            Console.WriteLine(big);
            Console.WriteLine("Is even: {0}", big.IsEven);
            Console.WriteLine("Is power of two: {0}", big.IsPowerOfTwo);

            BigInteger evenBigger = BigInteger.Add(big, BigInteger.Parse("34231553151352151"));

            Console.WriteLine(evenBigger);
            Console.WriteLine(evenBigger + big);
        }

        static void DateTypes()
        {
            DateTime dt = DateTime.Now;

            Console.WriteLine(dt);

            Console.WriteLine("Który miesiąc i ile ma dni: {0} ma {1} dni", dt.Month, DateTime.DaysInMonth(dt.Year, dt.Month));

            TimeSpan ts = new TimeSpan(6, 30, 25);

            Console.WriteLine(ts);

            Console.WriteLine(ts.Subtract(new TimeSpan(3, 12, 12)));
        }

        static void ParseFromStrings()
        {
            bool b = bool.Parse("true");

            Console.WriteLine(b);

            double d = double.Parse("999,232");

            Console.WriteLine(d);

            int i = int.Parse("8");

            Console.WriteLine(i);

            char c = char.Parse("w");

            Console.WriteLine(c);
        }

        static void BasicCharCapability()
        {
            char myChar = 'a';

            Console.WriteLine("mychar is a digit: {0}", char.IsDigit(myChar));

            Console.WriteLine("mychar is a letter: {0}", char.IsLetter(myChar));

            Console.WriteLine("mychar is a punctuation: {0}", char.IsPunctuation('?'));

            Console.WriteLine("is white space dla Hello World: {0}", char.IsWhiteSpace("Hello World", 5));

        }

        static void FormatowanieLiczb()
        {
            Console.WriteLine("Liczba 12345 w różnych formatach");

            Thread.CurrentThread.CurrentCulture = new CultureInfo("pl-PL");

            Console.WriteLine("format c (pieniezny) {0:c}, {1:c}", 12345, 2555);

            Console.WriteLine("format d (licz dziesietnych) {0:d9}, {1:d9}", 12345, 2555);

            Console.WriteLine("format f (po przecinku) {0:f4}, {1:f4}", 12345, 2555);

            Console.WriteLine("format n (podstawowy numeryczny) {0:n}, {1:n}", 12345, 2555);

            Console.WriteLine("format e (format wykladniczy) {0:e}, {1:e}", 12345, 2555);

            string heksadecimal = string.Format("format x (heksadecymalny) {0:x}, {1:x}", 12345, 2555);
            
            Console.WriteLine(heksadecimal);
        }
    }
}
