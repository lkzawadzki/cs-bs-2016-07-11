﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceBasic
{
    class Program
    {
        static void Main(string[] args)
        {
            //podstawy dziedziczenia

            Car car = new Car(60);

            car.Speed = 15;

            Console.WriteLine("Car speed is {0}", car.Speed);
            Console.WriteLine("Car max speed is {0}", car.maxSpeed);

            Sedan sedan = new Sedan();

            sedan.Speed = 60;

            Console.WriteLine("Sedan speed is {0}", sedan.Speed);
            Console.WriteLine("Sedan max speed is {0}", sedan.maxSpeed);

            Console.ReadLine();

        }
    }
}
