﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceBasic
{
    class Car
    {
        public int maxSpeed { get; private set; }

        private int currentSpeed;

        public Car()
        {
            maxSpeed = 20;
        }

        public Car(int maxSpeed)
        {
            this.maxSpeed = maxSpeed;
        }

        public int Speed
        {
            get
            {
                return currentSpeed;
            }
            set
            {
                currentSpeed = value;

                if (currentSpeed > maxSpeed)
                    currentSpeed = maxSpeed;
            }
        }
    }
}
