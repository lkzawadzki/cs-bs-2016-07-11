﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skladowe_statyczne
{
    class SavingsAccount : object
    {

        private double balance;

        private static double interestRate = 0.05;

        static SavingsAccount()
        {
            Console.WriteLine("W konstruktorze statycznym");
            interestRate = 0.09;
        }

        public SavingsAccount(double balance)
        {
            this.balance = balance;
        }

        public static double GetInterestRate()
        {
            return interestRate;
        }

        public static void SetInterestRate(double rate)
        {
            interestRate = rate;
        }
    }
}
