﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skladowe_statyczne
{
    class Program
    {
        static void Main(string[] args)
        {
            SavingsAccount firstSA = new SavingsAccount(250.00);

            SavingsAccount secondSA = new SavingsAccount(100.00);

            Console.WriteLine(SavingsAccount.GetInterestRate());

            SavingsAccount.SetInterestRate(0.07);

            Console.WriteLine(SavingsAccount.GetInterestRate());

            TimeWriter.Time();
            TimeWriter.Date();


            Console.ReadLine();
        }

        static class TimeWriter
        {
            public static void Time()
            {
                Console.WriteLine(DateTime.Now.ToShortTimeString());
            }

            public static void Date()
            {
                Console.WriteLine(DateTime.Now.ToShortDateString());
            }
        }
    }
}
