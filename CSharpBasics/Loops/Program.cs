﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
    partial class Program
    {
        static void Main(string[] args)
        {
            string con = "no";

            do
            {
                WhileExercise();

                Console.WriteLine("Czy chcesz zagrać jeszcze raz?");
                con = Console.ReadLine();

            } while (con.ToLower() == "yes");

            Console.ReadLine();
        }
    }
}
