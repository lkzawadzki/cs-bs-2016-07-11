﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Loops
{
    partial class Program
    {
        static void WhileExercise()
        {
            int twojNumer, numerKomputera;

            Console.WriteLine("Zgadnij numer komputera: ");

            twojNumer = int.Parse(Console.ReadLine());

            Random r = new Random();

            numerKomputera = r.Next(1, 5);

            if (twojNumer == numerKomputera)
            {
                Console.WriteLine("Wygrałeś!");
            }
            else
            {
                Console.WriteLine("Przegrałeś, bo numer to {0}", numerKomputera);
            }
        }

        static void WhileExample()
        {
            string s = "no";

            while (s.ToLower() != "yes")
            {
                Console.WriteLine("Czy chcesz wyjść z pętli? [yes]/[no]");
                s = Console.ReadLine();
            }

            do
            {
                Console.WriteLine("Czy chcesz wyjść z pętli? [yes]/[no]");
                s = Console.ReadLine();
            } while (s.ToLower() != "yes");
        }

        static void Foreach()
        {
            string[] countries = { "Portugal", "France", "Iceland" };

            foreach (string s in countries)
            {
                Console.WriteLine(s);
            }

            for (int i = 0; i < countries.Length; i++)
            {
                Console.WriteLine(countries[i]);
            }
        }

        static void ForExercise()
        {
            for (int i = 0; i <= 4; i++)
            {
                for (int j = 5; j >= i + 1; j--)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }
        }


        static void ForExample()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    for (int j = 0; j < 6; j++)
                    {
                        if (j == 3)
                        {
                            goto Outer;
                        }

                        Console.WriteLine("Pętla wewnętrzna: {0}", j);
                    }
                }

                Console.WriteLine("Numer pętli: {0}", i);
            }
            Outer:
            {
                Console.WriteLine("Breaking...");
            }
        }
    }
}
