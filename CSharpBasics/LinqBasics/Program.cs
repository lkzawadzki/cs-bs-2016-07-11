﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBasics
{
    class ClassWithLinqField
    {
        private static string[] countries = 
            { "Portugal", "France", "Wales", "Germany" };

        private IEnumerable<string> subset = 
            from c in countries where c.Contains("l") select c;

        public IEnumerable<string> GetCountries()
        {
            return subset.ToList();
        }

        public static void ChangeCountry(string country)
        {
            countries[0] = country;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            //StringArrayWithoutLinq();

            //StringArrayWithLinq();

            //ClassWithLinqField cwlf = new ClassWithLinqField();

            //var countries = cwlf.GetCountries();

            //foreach (var c in countries)
            //{
            //    Console.WriteLine(c);
            //}

            //ClassWithLinqField.ChangeCountry("Meksyk");

            //foreach (var c in countries)
            //{
            //    Console.WriteLine(c);
            //}

            List<Car> cars = new List<Car>()
            {
                new Car(1, "Mazda", 125000f, new DateTime(2012, 4, 20)),
                new Car(2, "Ford Mondeo", 55000f, new DateTime(2015, 12, 10)),
                new Car(3, "Maserati", 350000f, new DateTime(2016, 1, 1)),
                new Car(4, "Opel Astra", 90000f, new DateTime(2013, 10, 30))
            };

            List<Order> orders = new List<Order>()
            {
                new Order(1, 1, "Cash"),
                new Order(2, 3, "Credit card")
            };

            //GetExpensiveCars(cars, 200000f);

            //GetCarsInNongeneric();

            //GetCarNames(cars);

            //Array anonObjects = GetCarNames(cars);

            //foreach (object o in anonObjects)
            //{
            //    Console.WriteLine(o);
            //}

            //dlaczego nie działa?
            //for (int i = 0; i < anonObjects.Length; i++)
            //{
            //    Console.WriteLine(anonObjects.GetValue(i));
            //}

            //SortCars(cars, false);

            //SimpleSetOperations();

            LeftJoin(cars, orders);

            Console.ReadKey();
        }

        static void LeftJoin(List<Car> cars, List<Order> orders)
        {
            var lij = from c in cars
                      join o in orders
                      on c.CarId equals o.CarId
                      select new
                      {
                          c.CarId,
                          Name = c.Name,
                          o.PaymentMode
                      };

            foreach(var l in lij)
                Console.WriteLine(l);

            Console.WriteLine();

            var loj = from c in cars
                      join o in orders
                      on c.CarId equals o.CarId into details
                      from d in details.DefaultIfEmpty(new Order())
                      select new
                      {
                          c.CarId,
                          Name = c.Name,
                          d.PaymentMode
                      };

            foreach(var l in loj)
                Console.WriteLine(l);
        }

        static void SimpleSetOperations()
        {
            List<string> myCars = new List<string> { "Mazda", "BMW", "Mercedes" };
            List<string> yourCars = new List<string> { "Opel", "Mazda", "BMW" };

            //myCars - yourCars
            var diff = (from m in myCars select m).Except(from y in yourCars select y);

            foreach (string s in diff)
            {
                Console.Write(s + " ");
            }     
            Console.WriteLine();

            //myCars + yourCars (union)
            var union = (from m in myCars select m).Union(from y in yourCars select y);

            foreach (string s in union)
            {
                Console.Write(s + " ");
            }
            Console.WriteLine();

            //myCars + yourCars (union all)
            var concat = (from m in myCars select m).Concat(from y in yourCars select y);

            foreach (string s in concat.Distinct())
            {
                Console.Write(s + " ");
            }
            Console.WriteLine();

            //myCars & yourCars (union all)
            var intersect = (from m in myCars select m).Intersect(from y in yourCars select y);

            foreach (string s in intersect)
            {
                Console.Write(s + " ");
            }
            Console.WriteLine();

        }

        static void SortCars(List<Car> cars, bool ascending)
        {
            if (ascending)
            {
                foreach (Car c in cars.Select(c => c).OrderBy(c => c.Name))
                {
                    Console.WriteLine(c.Name);
                }
            }
            else
            {
                foreach (Car c in cars.Select(c => c).OrderByDescending(c => c.Name))
                {
                    Console.WriteLine(c.Name);
                }
            }
        }

        static Array GetCarNames(List<Car> cars)
        {
            //var names = from c in cars select new { c.Name, c.PurchaseDate };

            var names = cars.Select(c => new { c.Name, c.PurchaseDate });

            foreach (var n in names)
            {
                Console.WriteLine(n.Name + " " + n.PurchaseDate.ToShortDateString());
            }

            return names.ToArray();
        }

        static void GetCarsInNongeneric()
        {
            ArrayList cars = new ArrayList()
            {
                new Car(1, "Mazda", 125000f, new DateTime(2012, 4, 20)),
                new Car(2, "Ford Mondeo", 55000f, new DateTime(2015, 12, 10)),
                new Car(3, "Maserati", 350000f, new DateTime(2016, 1, 1)),
                new Car(4, "Opel Astra", 90000f, new DateTime(2013, 10, 30))
            };

            var compatibleCars = cars.OfType<Car>();

            var finalCars = compatibleCars.Where(c => c.Price > 70000f).Select(c => c);

            foreach (var car in finalCars)
            {
                Console.WriteLine(car.Name);
            }

        }

        static void GetExpensiveCars(List<Car> cars, float price)
        {
            var expensiveCars = cars.Where(c => c.Price > price).Select(c => c);

            foreach (var car in expensiveCars)
            {
                Console.WriteLine(car.Name);
            }
        }

        static void StringArrayWithLinq()
        {
            string[] movies = { "Braveheart", "Kingdom of Heaven", "Mission Impossible 3" };

            //IEnumerable<string> selectedMovies = from s in movies
            //                                     where s.Contains(" ")
            //                                     orderby s descending
            //                                     select s;

            IEnumerable<string> selectedMovies = movies
                                                    .Where(s => s.Contains(" "))
                                                    .OrderByDescending(s => s)
                                                    .Select(s => s).ToList();

            foreach (string s in selectedMovies)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine();

            movies[1] = "Frozen";

            foreach (string s in selectedMovies)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine();
        }

        static void StringArrayWithoutLinq()
        {
            string[] movies = { "Braveheart", "Kingdom of Heaven", "Mission Impossible 3" };

            string[] selectedMovies = new string[movies.Length];

            for (int i = 0; i < movies.Length; i++)
            {
                if (movies[i].Contains(" "))
                    selectedMovies[i] = movies[i];
            }

            Array.Sort(selectedMovies);

            Array.Reverse(selectedMovies);

            foreach (string s in selectedMovies)
            {
                if (s != null)
                    Console.WriteLine(s);
            }

            Console.WriteLine();
        }
    }
}
