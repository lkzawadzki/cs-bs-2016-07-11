﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqBasics
{
    class Car
    {
        public int CarId { get; set; }
        public string Name { get; set; }
        public float Price { get; set; }
        public DateTime PurchaseDate { get; set; }

        public Car(int id, string name, float price, DateTime date)
        {
            CarId = id;
            Name = name;
            Price = price;
            PurchaseDate = date;
        }
    }

    class Order
    {
        public int OrderId { get; set; }
        public int CarId { get; set; }
        public string PaymentMode { get; set; }

        public Order()
        {

        }

        public Order(int orderId, int carId, string paymentMode)
        {
            OrderId = orderId;
            CarId = carId;
            PaymentMode = paymentMode;
        }
    }
}
