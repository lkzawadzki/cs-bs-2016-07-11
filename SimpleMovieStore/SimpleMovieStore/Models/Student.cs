﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SimpleMovieStore.Models
{
    public class Student
    {
        public int StudentId { get; set; }

        [Required]
        [StringLength(25, MinimumLength = 3)]
        [Display(Name = "Imię i nazwisko")]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Display(Name = "Data urodzenia")]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Czy jest mężczyzną?")]
        public bool IsMale { get; set; }

        [Range(0, 1000)]
        [DataType(DataType.Currency)]
        [Display(Name = "Wysokość stypendium")]
        public decimal Grant { get; set; }
    }

    public class StudentExampleContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
    }
}