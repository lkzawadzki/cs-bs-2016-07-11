﻿using SimpleMovieStore.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleMovieStore.Controllers
{
    public class StudentsController : Controller
    {
        StudentExampleContext context = new StudentExampleContext();

        private string Message;

        // GET: Students
        public ActionResult Index(string sortOrder = "")
        {
            ViewBag.SortByName = String.IsNullOrEmpty(sortOrder) ? "Name_desc" : "";

            ViewBag.SortByDate = sortOrder == "Date" ? "Date_desc" : "Date";

            var students = context.Students.Select(s => s);

            switch (sortOrder)
            {
                case "Name_desc":
                    students = students.OrderByDescending(s => s.Name);
                    break;
                case "Date":
                    students = students.OrderBy(s => s.DateOfBirth);
                    break;
                case "Date_desc":
                    students = students.OrderByDescending(s => s.DateOfBirth);
                    break;
                default:
                    students = students.OrderBy(s => s.Name);
                    break;
            }


            return View(students);
        }

        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Student student)
        {
            if (ModelState.IsValid)
            {
                context.Students.Add(student);
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(student);
        }

        public ActionResult Details(int id)
        {
            Student student = context.Students.Find(id);

            if (student == null)
                return RedirectToAction("Index");

            return View(student);
        }

        public ActionResult Edit(int id)
        {
            Student student = context.Students.Find(id);

            if (student == null)
                return RedirectToAction("Index");

            return View(student);
        }

        [HttpPost]
        public ActionResult Edit(Student student)
        {
            if (ModelState.IsValid)
            {
                context.Entry(student).State = EntityState.Modified;
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(student);
        }

        public ActionResult Delete(int id)
        {
            Student student = context.Students.Find(id);

            if (student == null)
                return RedirectToAction("Index");

            return View(student);
        }

        [HttpPost]
        public ActionResult Delete(Student s, int id)
        {
            Student student = context.Students.Find(id);

            if (student == null)
                return RedirectToAction("Index");

            context.Students.Remove(student);
            context.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}