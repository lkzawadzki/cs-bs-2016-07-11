﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleMovieStore.Controllers
{
    public class HelloController : Controller
    {
        //// GET: Hello
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult MultipleWelcome(int numberOfWelcomings)
        {
            return View(numberOfWelcomings);
        }
    }
}